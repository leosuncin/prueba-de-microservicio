import itertools

def levenshtein_distance(string1, string2):
	"""
	Calculate the Levenshtein distance

	Parameters:
		string1 (str):The first string
		string2 (str):The second string

	Returns:
		(int):Levenshtein distance
	"""
	distance = 0

	if len(string1) < len(string2):
		string1, string2 = string2, string1

	for i, v in itertools.zip_longest(string1, string2, fillvalue="-"):
		if i != v:
			distance += 1

	return distance

if __name__ == "__main__":
    import sys
    print(levenshtein_distance(sys.argv[1], sys.argv[2]))
