from flask import Flask, request, jsonify
import levenshtein

app = Flask(__name__)


@app.route("/levenshtein", methods=["GET"])
def index():
	string1 = request.args.get("string1")
	string2 = request.args.get("string2")

	if None == string1 or 0 == len(string1):
		return "The string1 is required", 400

	if None == string2 or 0 == len(string2):
		return "The string2 is required", 400

	return jsonify(distance=levenshtein.levenshtein_distance(string1, string2))


if __name__ == "__main__":
	app.run(debug=True, port=3000)
