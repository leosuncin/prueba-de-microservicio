# Python service

## Requirements

- Python >= 3.6

## Instructions

Create a virtual environment

```bash
python3 -m venv venv
```

Activate the virtual environment

```bash
. ./venv/bin/activate
```

Install dependencies

```bash
pip install -r requirements.txt
```

Run the service

```bash
python main.py
```

## Documentation API

**URL:** /levenshtein

**Method:** GET

**Description:** Calculate the Levenshtein Distance between two strings

**Parameters:**

| Name    | Type   | Required | Description       |
| ------- | ------ | -------- | ----------------- |
| string1 | String | Yes      | The first string  |
| string2 | String | Yes      | The second string |

**Responses:**

_200 OK_

| Name     | Type    | Required | Description              |
| -------- | ------- | -------- | ------------------------ |
| distance | Integer | Yes      | The Levenshtein distance |

Example

```json
{
  "distance": 4
}
```

_400 Bad Request_

Example

> The string1 is required
