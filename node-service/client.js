const { get } = require('http');

/**
 * The payload of the remote
 *
 * @typedef {Object} Result
 * @property {number} distance The Levenshtein distance
 */

/**
 * Send request to calculate the Levenshtein distance
 *
 * @param {string} string1 First string
 * @param {string} string2 Second string
 * @returns {Promise<Result>}
 */
function requestCalc(string1, string2) {
  const url = process.env.SERVICE_URL || 'http://localhost:3000/levenshtein';

  return new Promise((resolve, reject) => {
    const request = get(
      `${url}?string1=${string1}&string2=${string2}`,
      response => {
        let data = [];

        response.on('data', chunk => {
          data.push(chunk);
        });

        response.on('end', () => {
          switch (response.statusCode) {
            case 200:
              const json = data.toString();
              resolve(JSON.parse(json));
              break;

            default:
              reject(new Error(data.toString()));
              break;
          }
        });
      }
    );

    request.on('error', reject);

    request.end();
  });
}

module.exports = { requestCalc };
